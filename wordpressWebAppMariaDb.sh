#!/bin/bash

# Mise à jour de Debian pour avoir tous les packages à jour


apt update && apt upgrade -y

#On se met dans le dossier site/wwwroot

cd /home/site/wwwroot

#On télécharge wordpress

wget -c http://WordPress.org/latest.tar.gz

# On décompresse et on se place dans le dossier wwwroot

tar -xzvf latest.tar.gz

# On le déplace et ensuite on supprime les fichiers inutile 

mv WordPress/* /home/site/wwwroot/

rm -rf WordPress

rm -rf latest.tar.gz
